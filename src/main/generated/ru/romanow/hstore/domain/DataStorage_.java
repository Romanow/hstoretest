package ru.romanow.hstore.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.ArrayList;
import java.util.HashMap;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(DataStorage.class)
public abstract class DataStorage_ {

	public static volatile SingularAttribute<DataStorage, HashMap> data;
	public static volatile SingularAttribute<DataStorage, String> name;
	public static volatile SingularAttribute<DataStorage, Integer> id;
	public static volatile SingularAttribute<DataStorage, ArrayList> countries;

}

