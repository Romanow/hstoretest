package ru.romanow.hstore.domain;

import com.google.common.base.MoreObjects;
import ru.romanow.hstore.domain.converter.ArrayConverter;
import ru.romanow.hstore.domain.converter.MapConverter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ronin on 20.06.15
 */
@Entity
@Table(name = "data_storage")
public class DataStorage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name", length = 80)
    private String name;

    @Basic
    @Convert(converter = MapConverter.class)
    @Column(name = "data")
    private HashMap<String, Object> data;

    @Basic
    @Convert(converter = ArrayConverter.class)
    @Column(name = "countries")
    private ArrayList<String> countries;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HashMap<String, Object> getData() {
        return data;
    }

    public void setData(HashMap<String, Object> data) {
        this.data = data;
    }

    public ArrayList<String> getCountries() {
        return countries;
    }

    public void setCountries(ArrayList<String> countries) {
        this.countries = countries;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("name", name)
                .add("data", data)
                .add("countries", countries)
                .toString();
    }
}
