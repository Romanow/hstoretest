package ru.romanow.hstore.domain.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.HashMap;

/**
 * Created by ronin on 20.06.15
 */
@Converter
public class MapConverter
        implements AttributeConverter<HashMap, Object> {

    @Override
    public Object convertToDatabaseColumn(HashMap attribute) {
        return attribute;
    }

    @Override
    public HashMap convertToEntityAttribute(Object data) {
        return (HashMap)data;
    }
}
