package ru.romanow.hstore.domain.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ronin on 27.06.15
 */
@Converter
public class ArrayConverter
        implements AttributeConverter<ArrayList, String[]> {

    @Override
    @SuppressWarnings("unchecked")
    public String[] convertToDatabaseColumn(ArrayList list) {
        return list != null ?
                ((List<String>)list).toArray(new String[list.size()]) :
                new String[0];
    }

    @Override
    public ArrayList convertToEntityAttribute(String[] strings) {
        return new ArrayList<>(Arrays.asList(strings));
    }
}
