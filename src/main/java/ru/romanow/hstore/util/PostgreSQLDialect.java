package ru.romanow.hstore.util;

import org.eclipse.persistence.expressions.ExpressionOperator;
import org.eclipse.persistence.internal.helper.ClassConstants;
import org.eclipse.persistence.internal.helper.NonSynchronizedVector;
import org.eclipse.persistence.platform.database.PostgreSQLPlatform;

import java.util.Vector;

/**
 * Created by ronin on 27.06.15
 */
public class PostgreSQLDialect extends PostgreSQLPlatform {

    @Override
    public void initialize() {
        super.initialize();

        ExpressionOperator operator = new ExpressionOperator();
        operator.setName("hkey");
        operator.setIsBindingSupported(true);
        operator.setType(ExpressionOperator.FunctionOperator);
        operator.setSelector(1001);
        Vector v = NonSynchronizedVector.newInstance(3);
        v.add("");
        v.add(" -> ");
        v.add("");
        operator.printsAs(v);
        operator.bePrefix();
        operator.setNodeClass(ClassConstants.FunctionExpression_Class);

        addOperator(operator);
        ExpressionOperator.registerOperator(1001, "hkey");
    }
}
