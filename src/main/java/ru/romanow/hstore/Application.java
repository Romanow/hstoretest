package ru.romanow.hstore;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.romanow.hstore.service.DataStorageService;

/**
 * Created by ronin on 20.06.15
 */
public class Application {
    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml");
        DataStorageService service = context.getBean(DataStorageService.class);
//        DataStorage data = service.getDataStorageById(4);
//
//        logger.info("{}", data);

//        data.setData(new HashMap<>());
//        service.updateDataStorage(data);
//        logger.info("Count {}", service.getTotalCount());

//        service.getAllDataStorage()
//                .stream()
//                .forEach(d -> logger.info("{}", d));

        service.testOperator();
    }
}
