package ru.romanow.hstore.repository;

import ru.romanow.hstore.domain.DataStorage;

import java.util.List;

/**
 * Created by ronin on 27.06.15
 */
public interface DataStorageDAO {
    List<DataStorage> findAll();

    DataStorage findOne(Integer id);

    Long getTotalCount();
}
