package ru.romanow.hstore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.romanow.hstore.domain.DataStorage;

import java.util.List;

/**
 * Created by ronin on 20.06.15
 */
@Repository
public interface DataStorageRepository
        extends JpaRepository<DataStorage, Integer>,
                JpaSpecificationExecutor<DataStorage> {

//        @Query("select d from DataStorage d order by operator('hkey', d.data, 'ru')")
        @Query("select d from DataStorage d where function('sort', d.data, 'ru') > 3")
        List<Integer> getList();
}
