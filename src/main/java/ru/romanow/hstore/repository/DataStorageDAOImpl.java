package ru.romanow.hstore.repository;

import org.springframework.stereotype.Repository;
import ru.romanow.hstore.domain.DataStorage;
import ru.romanow.hstore.domain.DataStorage_;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by ronin on 27.06.15
 */
@Repository
public class DataStorageDAOImpl implements DataStorageDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<DataStorage> findAll() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<DataStorage> criteriaQuery = criteriaBuilder.createQuery(DataStorage.class);

        Root<DataStorage> root = criteriaQuery.from(DataStorage.class);

        return entityManager.createQuery(criteriaQuery.select(root)).getResultList();
    }

    @Override
    public DataStorage findOne(Integer id) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<DataStorage> criteriaQuery = criteriaBuilder.createQuery(DataStorage.class);

        Root<DataStorage> root = criteriaQuery.from(DataStorage.class);

        return entityManager.createQuery(
                criteriaQuery.select(root)
                        .where(criteriaBuilder.equal(root.get(DataStorage_.id), id))
        ).getSingleResult();
    }

    @Override
    public Long getTotalCount() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);

        Root<DataStorage> root = criteriaQuery.from(DataStorage.class);

        return entityManager.createQuery(
                criteriaQuery.select(criteriaBuilder.count(root.get(DataStorage_.id)))
        ).getSingleResult();    }
}
