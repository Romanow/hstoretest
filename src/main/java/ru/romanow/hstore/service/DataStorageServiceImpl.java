package ru.romanow.hstore.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.expression.Expression;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.romanow.hstore.domain.DataStorage;
import ru.romanow.hstore.domain.DataStorage_;
import ru.romanow.hstore.repository.DataStorageDAO;
import ru.romanow.hstore.repository.DataStorageRepository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by ronin on 20.06.15
 */
@Service
public class DataStorageServiceImpl
        implements DataStorageService {

    private static final Logger logger = LoggerFactory.getLogger(DataStorageServiceImpl.class);

    private static final Specification<DataStorage> displayOrderSpecification =
            (root, query, cb) -> {
                query.orderBy(
                        cb.asc(
                                cb.function("sort", Integer.class, root.get(DataStorage_.data))
                        )
                );

                return null;
            };

    @Autowired
    private DataStorageRepository dataStorageRepository;

    @Autowired
    private DataStorageDAO dataStorageDAO;

    @Override
    @Transactional(readOnly = true)
    public List<DataStorage> getAllDataStorage() {
        return dataStorageDAO.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Long getTotalCount() {
        return dataStorageDAO.getTotalCount();
    }

    @Override
    @Transactional(readOnly = true)
    public DataStorage getDataStorageById(Integer id) {
        return dataStorageDAO.findOne(id);
    }

    /**
     * [DataStorage{name=Ukrain, data={ru=1, ua=1, us=1}, countries=[RU, UK]},
     *  DataStorage{name=Mexico, data={ru=2, us=1}, countries=[RU, UK]},
     *  DataStorage{name=USA, data={ru=3, ua=1, us=1}, countries=[RU, UK]},
     *  DataStorage{name=Russia, data={}, countries=[RU, UK]}]
     *
     * [DataStorage{name=Ukrain, data={ru=1, ua=1, us=1}, countries=[RU, UK]},
     *  DataStorage{name=USA, data={ru=3, ua=1, us=1}, countries=[RU, UK]},
     *  DataStorage{name=Mexico, data={ru=2, us=1}, countries=[RU, UK]},
     *  DataStorage{name=Russia, data={}, countries=[RU, UK]}]
     */

    @Override
    @Transactional(readOnly = true)
    public void testOperator() {
        logger.info("{}", dataStorageRepository.findAll(displayOrderSpecification));
    }

    @Override
    @Transactional
    public DataStorage updateDataStorage(DataStorage data) {
        return dataStorageRepository.save(data);
    }
}
