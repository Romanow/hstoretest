package ru.romanow.hstore.service;

import ru.romanow.hstore.domain.DataStorage;

import java.util.List;

/**
 * Created by ronin on 20.06.15
 */
public interface DataStorageService {
    List<DataStorage> getAllDataStorage();

    Long getTotalCount();

    DataStorage getDataStorageById(Integer id);

    void testOperator();

    DataStorage updateDataStorage(DataStorage data);
}
